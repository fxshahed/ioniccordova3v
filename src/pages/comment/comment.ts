import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comment.html',
})
export class CommentsPage {

  private writeComments : FormGroup;

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, 
    public navParams: NavParams, private formBuilder: FormBuilder) {
    this.writeComments = this.formBuilder.group({

      author: ['', Validators.required],
      rating:['',Validators.required],
      comment: ['',Validators.required],

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addComments(){
    console.log(this.writeComments.value);
    this.viewCtrl.dismiss(this.writeComments.value);
  }

}
