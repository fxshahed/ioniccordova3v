import { Component, Inject } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController, IonicPage } from 'ionic-angular';
import { Dish } from "../../shared/dish";
import { Comment } from "../../shared/comment";

import { DatePipe } from "@angular/common";
import { FavoriteProvider } from "../../providers/favorite/favorite";
import { CommentsPage } from '../comment/comment';



@IonicPage()
@Component({
  selector: 'page-dishdetail',
  templateUrl: 'dishdetail.html',
})
export class DishdetailPage {
  dish: Dish;
  errMess: string;
  avgstars: string;
  numcomments: number;
  newComment: Comment ={
    rating: 0,
    author:'',
    comment:'',
    date:''
  };
  constructor(private favoriteservice : FavoriteProvider, private datePipe: DatePipe, public modalCtrl: ModalController,
    @Inject('BaseURL') public BaseURL, public navCtrl: NavController, 
    public navParams: NavParams, public actionSheetCtrl: ActionSheetController) {

    this.dish = navParams.get('dish');

    this.numcomments = this.dish.comments.length;
    let total = 0;

    this.dish.comments.forEach(comment => total += comment.rating );

    this.avgstars = (total/this.numcomments).toFixed(2);
    
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad DishdetailPage');
  }

  openOptions(){
    this.presentActionSheet();
  }
  presentActionSheet() {
   let actionSheet = this.actionSheetCtrl.create({
     title: 'Select Actions',
     buttons: [
       {
         text: 'Add to Favorites',
         handler: () => {
           console.log('Add to Favorites clicked');
           this.addToFavorites(this.dish);
         }
       },
       {
         text: 'Add comment',
         handler: () => {
           console.log('Add comment clicked');
           this.presentModal();
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   actionSheet.present();
 }
 presentModal() {
    const modal = this.modalCtrl.create(CommentsPage);
    modal.present();
    modal.onDidDismiss((comments)=>{
      if(comments){
        this.newComment = {
          rating: 0,
          author:'',
          comment:'',
          date:''
        };
        this.addComments(comments);
      }
    });
  }

  addComments(comments){
    let date = Date.now();
    let newDate : string;
    newDate = this.datePipe.transform(date, 'yyyy-MM-dd h:mm:ss a');
    this.datePipe.transform(date, 'yyyy-MM-dd');
    this.newComment.rating = comments.rating;
    this.newComment.comment = comments.comment;
    this.newComment.author = comments.author;
    this.newComment.date = newDate;
    this.dish.comments.push(this.newComment);
    this.numcomments = this.dish.comments.length;
    let total = 0;
    this.dish.comments.forEach(comment => total += comment.rating );
    this.avgstars = (total/this.numcomments).toFixed(2);
  }

  addToFavorites(dish: Dish) {
    console.log('Adding to Favorites', dish.id);
    this.favoriteservice.addFavorite(dish.id);
  }

}
